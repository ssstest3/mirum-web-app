import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../styles/page5.css";
import fb from "../../assets/fb.png";
import twitter from "../../assets/twitter.png";
import youtube from "../../assets/youtube.png";
import linkedin from "../../assets/linkedin.png";

class page5 extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div className="Page5">
        <div className="div51">
          <img src={fb} alt="fb" />
          <img src={twitter} alt="twtr" />
          <img src={youtube} alt="ytb" />
          <img src={linkedin} alt="lkdn" />
        </div>
        <div className="div52">
          <p className="p">Disclaimer | Terms of Use & Privacy Notice</p>
          <p className="p">&#169;2020 MIRUM. All Rights Reserved</p>
        </div>
        <div className="div53"></div>
      </div>
    );
  }
}
export default page5;
