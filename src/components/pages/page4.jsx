import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../styles/page4.css";
import related1 from "../../assets/related-1.png";
import related2 from "../../assets/related-2.png";

class page4 extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div className="Page4">
        <div className="div4">
          <img src={related1} alt="related1" />
          <p>SPACE IPSUM</p>
        </div>

        <h1 className="label">RELATED</h1>
        <div className="div4">
          <img src={related2} alt="related2" />
          <p>ZOMBIE IPSUM</p>
        </div>
      </div>
    );
  }
}
export default page4;
