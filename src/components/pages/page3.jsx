import React, { Component } from "react";
import "../../styles/page3.css";
import lapy from "../../assets/article-thumb.png";
import articles from "../../utils/articles.json";

class page3 extends Component {
  constructor() {
    super();
    this.state = {
      visible: 2,
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((old) => {
      return { visible: old.visible + 2 };
    });
  }
  render() {
    return (
      <div className="Page3">
        <div className="div31">
          <h1>ARTICLES</h1>
        </div>
        <div className="div32">
          <div className="div321">
            <img src={lapy} alt="lapy" />
          </div>
          <div className="div322">
            <h1>What is Lorem Ipsum?</h1>
            {articles.slice(0, this.state.visible).map((art) => (
              <p>{art.para}</p>
            ))}
          </div>
        </div>
        <div className="div33">
          <button onClick={this.loadMore}> READ MORE ...</button>
        </div>
      </div>
    );
  }
}
export default page3;
