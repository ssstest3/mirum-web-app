import React, { useState } from "react";
import "../../styles/page2.css";
import lightleft from "../../assets/cal-left-poll.png";
import lightright from "../../assets/cal-right-poll.png";
import cloud from "../../assets/cloud.png";
import calcleft from "../../assets/cal-stick.png";

const Page2 = () => {
  const [formData, setFormData] = useState({
    year: "",
    rate: "",
    amount: "",
  });

  const { year, rate, amount } = formData;
  const [total, setTotal] = useState();

  const caculateTotal = (e) => {
    const presentVal = parseFloat(amount);
    const intRate = parseFloat(rate) / 100;
    const years = parseFloat(year);
    const futureVal = presentVal * Math.pow(1.0 + intRate, years);
    setTotal(futureVal.toFixed(2));
  };

  const validate = (evt) => {
    var theEvent = evt || window.event;

    // Handle key press
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);

    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  };
  const onHandleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <div className="Page2">
      <div className="div21">
        <img className="cloudleft" src={cloud} alt="cloud" />
        <img className="lightleft" src={lightleft} alt="lightleft" />
      </div>
      <div className="div22">
        <div className="div221">
          <h1>THE LORUM IPSUM</h1>
          <h1>CALCULATOR</h1>
        </div>

        <div className="div222">
          <div className="calculator">
            <div className="amount">
              <div className="amountLabel">AMOUNT</div>
              <hr className="hl" />
              <input
                type="text"
                onKeyPress={(e) => validate(e)}
                name="amount"
                value={amount}
                onChange={(e) => onHandleChange(e)}
              />
            </div>
            <div className="terms">
              <div className="termsLabel">TERM</div>
              <hr className="hl" style={{ marginTop: "auto" }} />
              <input
                type="text"
                onKeyPress={(e) => validate(e)}
                name="year"
                value={year}
                onChange={(e) => onHandleChange(e)}
              />
              <div className="yearsLabel">YEARS</div>

              <hr className="hl" style={{ marginTop: "auto" }} />
              <div className="rateLabel">RATE</div>
              <input
                type="text"
                onKeyPress={(e) => validate(e)}
                name="rate"
                value={rate}
                onChange={(e) => onHandleChange(e)}
              />
              <div className="rateLabel">%</div>
            </div>
            <div class="vl"></div>
            <div className="calculate">
              <button onClick={(e) => caculateTotal(e)}>CALCULATE</button>
            </div>

            <div className="total">
              <div className="totalLabel">TOTAL</div>
              <hr className="hl" style={{ marginTop: "auto" }} />
              <label>{total}</label>
            </div>
          </div>
        </div>
        <div className="div223">
          <img className="calcpoll1" src={calcleft} alt="calcleft" />
          <img className="calcpoll2" src={calcleft} alt="calcright" />
        </div>
      </div>
      <div className="div23">
        <img className="cloudright" src={cloud} alt="cloud" />
        <img className="lightright" src={lightright} alt="lightright" />
      </div>
    </div>
  );
};

export default Page2;
