import React, { Component } from "react";
import "../../styles/page1.css";
import img1 from "../../assets/character.png";
import cloud from "../../assets/cloud.png";
import logo from "../../assets/logo.png";
import play from "../../assets/play.png";
import ModalVideo from "react-modal-video";

export default class page1 extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
    };
    this.openModal = this.openModal.bind(this);
  }

  openModal() {
    this.setState({ isOpen: true });
  }
  render() {
    return (
      <div className="Page1">
        <div className="div11">
          <img src={cloud} alt="cloud" />
        </div>
        <div className="div14">
          <h1>LOREM IPSUM</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor
          </p>
          <div className="thumbnail">
            <ModalVideo
              channel="youtube"
              isOpen={this.state.isOpen}
              videoId="Z7UnPdLJMZU"
              onClose={() => this.setState({ isOpen: false })}
            />
            <button className="palybutton" onClick={this.openModal}>
              <img classname="play" src={play} alt="play" />
            </button>
          </div>
        </div>

        <div className="div13">
          <img src={img1} alt="img1" />
        </div>

        <div className="div15">
          <img className="logo" src={logo} alt="logo" />
          <img className="cloud" src={cloud} alt="cloud" />
        </div>
      </div>
    );
  }
}
