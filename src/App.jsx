import React from "react";
import Page1 from "./components/pages/page1";
import Page2 from "./components/pages/page2";
import Page3 from "./components/pages/page3";
import Page4 from "./components/pages/page4";
import Page5 from "./components/pages/page5";

function App() {
  return (
    <div className="App">
      <Page1 />
      <Page2 />
      <Page3 />
      <Page4 />
      <Page5 />
    </div>
  );
}

export default App;
